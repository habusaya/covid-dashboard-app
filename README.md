# สถานการณ์ โควิด-19

รายงานสถานการณ์ โควิด-19 จากเว็ของกรมควบคุมโรค (https://covid19.ddc.moph.go.th) ผ่าน API https://covid19.ddc.moph.go.th/th/api 

## Getting Started

พัฒนาด้วย Flutter มีเฉพาะโค้ดที่จำเป็นนะครับ ให้สร้าง project ใหม่แล้ว เอา libs, pubspec.yaml ไปใช้งานต่อนะครับ

```
git clone https://gitlab.com/siteslave/covid-dashboard-app.git
```

## ตัวอย่าง

![](ss/ss1.jpg)