import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

class Helper {
  Helper();

  alert(String message) {
    Fluttertoast.showToast(
        msg: "$message",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  String toThaiDate(DateTime date) {
    // create format
    var strDate = new DateFormat.MMMd('th_TH').format(date);
    var _strDate = '$strDate ${date.year + 543}';
    // return thai date
    return _strDate;
  }

  String toLongThaiDate(DateTime date) {
    // create format
    var strDate = new DateFormat.MMMMd('th_TH').format(date);
    var _strDate = '$strDate ${date.year + 543}';
    // return thai date
    return _strDate;
  }

  String toLongThaiDateAndTime(DateTime date) {
    // create format
    var strDate = new DateFormat.MMMMd('th_TH').format(date);
    var strTime = new DateFormat.Hms().format(date);
    var _strDate = '$strDate ${date.year + 543} $strTime น.';
    // return thai date
    return _strDate;
  }

  String timestampToTime(DateTime date) {
    // create format
    var strDate = new DateFormat.Hm('th_TH').format(date);
    // return thai date
    return strDate;
  }

  String formatNumber(int number, int length) {
    try {
      if (length == 1) {
        var f1 = new NumberFormat("###,##0.0", "en_US");
        return f1.format(number);
      } else if (length == 2) {
        var f1 = new NumberFormat("###,##0.00", "en_US");
        return f1.format(number);
      } else if (length == 3) {
        var f1 = new NumberFormat("###,##0.000", "en_US");
        return f1.format(number);
      } else {
        var f1 = new NumberFormat("###,###", "en_US");
        return f1.format(number);
      }
    } catch (error) {
      return '0';
    }
  }
}
